
import java.util.Scanner;

public class Punto3 {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Ingrese un mes  ");
			int quemes= sc.nextInt();		
			while(quemes < 1 || quemes > 12) {
				System.out.println("Ese numero es invalido elige otro mes" );
				System.out.println("Ingrese un mes ");
				 quemes= sc.nextInt();
			}
			
			if(quemes == 1) {
				System.out.println("El mes selecionado es Enero y tiene " + 31 + " dias");
			}
			if(quemes == 2) {
				System.out.println("El mes selecionado es Febrero y tiene " + 28 + " dias");
			}
			if(quemes == 3) {
				System.out.println("El mes selecionado es Marzo y tiene " + 31 + " dias");
			}
			if(quemes == 4) {
				System.out.println("El mes selecionado es Abril y tiene " + 30 + " dias");
			}
			if(quemes == 5) {
				System.out.println("El mes selecionado es Mayo y tiene " + 31 + " dias");
			}
			if(quemes == 6) {
				System.out.println("El mes selecionado es Jumio y tiene " + 30 + " dias");
			}
			if(quemes == 7) {
				System.out.println("El mes selecionado es Julio y tiene " + 31 + " dias");
			}
			if(quemes == 8) {
				System.out.println("El mes selecionado es Agosto y tiene " + 31 + " dias");
			}
			if(quemes == 9) {
				System.out.println("El mes selecionado es Septiembre y tiene " + 30 + " dias");
			}
			if(quemes == 10) {
				System.out.println("El mes selecionado es Octubre y tiene " + 31 + " dias");
			}
			if(quemes == 11) {
				System.out.println("El mes selecionado es Noviembre y tiene " + 30 + " dias");
			}
			if(quemes == 12) {
				System.out.println("El mes selecionado es Diciembre y tiene " + 31 + " dias");
			}
			
			
	}

}
}
